package firstproblem;

import java.text.NumberFormat;

public class FirstProblem {

	public static void main(String[] args) {
		
		int sumOfNumbers = 0;
		
		for(int i = 1; i < 1000; i++) {
	        int currentNumber = i;
	        int firstMultiple = 3;
	        int secondMultiple = 5;
	        
	        if((currentNumber % firstMultiple) == 0 || (currentNumber % secondMultiple) == 0) {
	        	sumOfNumbers = sumOfNumbers + currentNumber;
	        	
	        }

		}
		
		System.out.println("The sum of all multiples of 3 and 5 to a thousand is: " + NumberFormat.getInstance().format(sumOfNumbers));

	}

}
